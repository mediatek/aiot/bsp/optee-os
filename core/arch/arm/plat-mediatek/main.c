// SPDX-License-Identifier: BSD-2-Clause
/*
 * Copyright (c) 2015, Linaro Limited
 */

#include <console.h>
#include <drivers/gic.h>
#include <drivers/serial8250_uart.h>
#include <kernel/boot.h>
#include <kernel/panic.h>
#include <kernel/tee_common_otp.h>
#include <mm/core_memprot.h>
#include <platform_config.h>
#include <stdint.h>
#include <string.h>

#define MTK_HW_UNIQUE_KEY_LENGTH	4
#define MTK_SIP_GET_HUK			0x82000010

#if defined(CFG_UART_ENABLE)
register_phys_mem_pgdir(MEM_AREA_IO_NSEC,
			CONSOLE_UART_BASE, SERIAL8250_UART_REG_SIZE);
#endif

static struct serial8250_uart_data console_data;

register_ddr(CFG_DRAM_BASE, CFG_DRAM_SIZE);

#ifdef CFG_GIC
static struct gic_data gic_data;

register_phys_mem_pgdir(MEM_AREA_IO_SEC, GIC_BASE + GICD_OFFSET,
			CORE_MMU_PGDIR_SIZE);
register_phys_mem_pgdir(MEM_AREA_IO_SEC, GIC_BASE + GICC_OFFSET,
			CORE_MMU_PGDIR_SIZE);

void main_init_gic(void)
{
	vaddr_t gicc_base = 0;
	vaddr_t gicd_base = 0;

	gicc_base = (vaddr_t)phys_to_virt(GIC_BASE + GICC_OFFSET,
					  MEM_AREA_IO_SEC, 1);
	gicd_base = (vaddr_t)phys_to_virt(GIC_BASE + GICD_OFFSET,
					  MEM_AREA_IO_SEC, 1);
	if (!gicc_base || !gicd_base)
		panic();

	gic_init_base_addr(&gic_data, gicc_base, gicd_base);

	itr_init(&gic_data.chip);
}

void itr_core_handler(void)
{
	gic_it_handle(&gic_data);
}
#endif

void console_init(void)
{
#if defined(CFG_UART_ENABLE)
	serial8250_uart_init(&console_data, CONSOLE_UART_BASE,
			     CONSOLE_UART_CLK_IN_HZ, CONSOLE_BAUDRATE);
	register_serial_console(&console_data.chip);
#endif
}

static int mtk_get_huk(uint32_t *key) {
	struct thread_smc_args args = {
		.a0 = MTK_SIP_GET_HUK,
	};

	thread_smccc(&args);

	if (args.a0 != MTK_HW_UNIQUE_KEY_LENGTH) {
		EMSG("Unsupported HUK size\n");
		return 0;
	}

	key[0] = args.a1;
	key[1] = args.a2;
	key[2] = args.a3;
	key[3] = args.a4;

	return args.a0;
}

TEE_Result tee_otp_get_hw_unique_key(struct tee_hw_unique_key *hwkey)
{
	uint32_t key[MTK_HW_UNIQUE_KEY_LENGTH];
	int size;

	memset(hwkey->data, 0, HW_UNIQUE_KEY_LENGTH);

	size = mtk_get_huk(key);
	if (!size)
		return TEE_ERROR_NO_DATA;

	memcpy(hwkey->data, key, HW_UNIQUE_KEY_LENGTH);

	return TEE_SUCCESS;
}
